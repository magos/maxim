import { Component } from '@angular/core';

@Component({
  selector: 'mx-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  isOpen: boolean;

  constructor() {
    this.isOpen = false;
  }
}
