import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'mx-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() applyFilter: EventEmitter<string>;
  searchTerm$: Subject<string>;

  constructor() {
    this.applyFilter = new EventEmitter<string>();
    this.searchTerm$ = new Subject<string>();
  }

  ngOnInit() {
    this.searchTerm$.pipe(debounceTime(200), distinctUntilChanged())
      .subscribe((searchTerm: string) => {
        this.applyFilter.emit(searchTerm);
      });
  }
}
