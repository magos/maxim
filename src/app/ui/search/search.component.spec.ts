import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render search input element', () => {
    const searchInput = de.query(By.css('#search-input'));
    expect(searchInput).toBeTruthy();
  });

  it('should update search term with correct value on key up input event', () => {
    const searchInput = de.query(By.css('#search-input'));
    component.searchTerm$.subscribe((searchTerm) => {
      expect(searchTerm).toBe('Luke');
    });
    searchInput.triggerEventHandler('keyup', { target: { value: 'Luke' } });
  });

  it('should emit applyFilter event with correct value on key up input event', fakeAsync(() => {
    const searchInput = de.query(By.css('#search-input'));
    component.applyFilter.subscribe((searchTerm) => {
      expect(searchTerm).toBe('Darth');
    });
    searchInput.triggerEventHandler('keyup', { target: { value: 'Darth' } });
    tick(200);
  }));
});
