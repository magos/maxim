import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { PaginationComponent } from './pagination.component';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginationComponent ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('should create the pagination', () => {
    expect(component).toBeTruthy();
  });

  it('should render previous button', () => {
    component.total = 5;
    fixture.detectChanges();
    const previousButton: DebugElement = de.query(By.css('#mx-previous-button'));
    expect(previousButton).toBeTruthy();
  });

  it('should render next button', () => {
    component.total = 5;
    fixture.detectChanges();
    const nextButton: DebugElement = de.query(By.css('#mx-next-button'));
    expect(nextButton).toBeTruthy();
  });

  it('should have appropriate number of pages after initialization', () => {
    component.total = 38;
    fixture.detectChanges();
    expect(component.pages.length).toBe(4);
  });

  it('should render appropriate number of pagination nav buttons', () => {
    component.total = 38;
    fixture.detectChanges();
    const numericButtons: DebugElement[] = de.queryAll(By.css('.mx-item-number-button'));
    expect(numericButtons.length).toBe(4);
  });

  it('should highlight first page by default', () => {
    component.total = 38;
    fixture.detectChanges();
    const numericButtons: DebugElement[] = de.queryAll(By.css('.mx-item-number-button'));
    expect(numericButtons[0].classes['active']).toBeTruthy();
  });

  it('should highlight appropriate page regarding input', () => {
    component.total = 45;
    component.currentPage = 3;
    fixture.detectChanges();
    const numericButtons: DebugElement[] = de.queryAll(By.css('.mx-item-number-button'));
    expect(numericButtons[2].classes['active']).toBeTruthy();
  });

  it('should emit pageChange event on page button click with appropriate payload', () => {
    component.total = 45;
    fixture.detectChanges();
    const numericButtons: DebugElement[] = de.queryAll(By.css('.mx-item-number-button'));
    component.pageChanged.subscribe((page) => {
      expect(page).toBe(3);
    });
    numericButtons[2].triggerEventHandler('click', 2);
  });

  it('should disable previous button in case of the first page selected', () => {
    component.total = 12;
    component.currentPage = 1;
    fixture.detectChanges();
    const previousButton: DebugElement = de.query(By.css('#mx-previous-button'));
    expect(previousButton.classes['disabled']).toBeTruthy();
  });

  it('should disable previous button in case of the last page selected', () => {
    component.total = 24;
    component.currentPage = 3;
    fixture.detectChanges();
    const nextButton: DebugElement = de.query(By.css('#mx-next-button'));
    expect(nextButton.classes['disabled']).toBeTruthy();
  });

  describe('changePage', () => {
    it('should not emit pageChange event when parameter value is negative', () => {
      spyOn(component.pageChanged, 'emit');
      component.total = 45;
      fixture.detectChanges();
      component.changePage(-2);
      expect(component.pageChanged.emit).not.toHaveBeenCalled();
    });

    it('should not emit pageChange event when parameter value is zero', () => {
      spyOn(component.pageChanged, 'emit');
      component.total = 45;
      fixture.detectChanges();
      component.changePage(0);
      expect(component.pageChanged.emit).not.toHaveBeenCalled();
    });

    it('should not emit pageChange event when parameter value exceedes number of pages', () => {
      spyOn(component.pageChanged, 'emit');
      component.total = 45;
      fixture.detectChanges();
      component.changePage(6);
      expect(component.pageChanged.emit).not.toHaveBeenCalled();
    });

    it('should emit pageChange event when parameter value is in current page range', () => {
      spyOn(component.pageChanged, 'emit');
      component.total = 45;
      fixture.detectChanges();
      component.changePage(4);
      expect(component.pageChanged.emit).toHaveBeenCalledWith(4);
    });
  });

  describe('isFirstPage', () => {
    it('should return true when the first page is set', () => {
      component.total = 15;
      component.currentPage = 1;
      fixture.detectChanges();
      expect(component.isFirstPage).toBeTruthy();
    });

    it('should return false when the second page is set', () => {
      component.total = 15;
      component.currentPage = 2;
      fixture.detectChanges();
      expect(component.isFirstPage).toBeFalsy();
    });
  });

  describe('isLastPage', () => {
    it('should return true when the last page is set', () => {
      component.total = 35;
      component.currentPage = 4;
      fixture.detectChanges();
      expect(component.isLastPage).toBeTruthy();
    });

    it('should return false when the last page is not set', () => {
      component.total = 35;
      component.currentPage = 2;
      fixture.detectChanges();
      expect(component.isLastPage).toBeFalsy();
    });
  });
});
