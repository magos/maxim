import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'mx-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() currentPage = 1;
  @Input() pageSize = 10;
  @Input() total = 0;
  @Output() pageChanged: EventEmitter<number>;
  pages: Array<number>;

  get isFirstPage(): boolean {
    return this.currentPage === this.pages[0];
  }

  get isLastPage(): boolean {
    return this.currentPage === this.pages[this.pages.length - 1];
  }

  constructor() {
    this.pageChanged = new EventEmitter<number>();
  }

  ngOnInit() {
    const numberOfPages = Math.ceil(this.total / this.pageSize);
    this.pages = Array(numberOfPages).fill(0).map((item, index) => ++index);
  }

  changePage(page: number) {
    if (page > 0 && page <= this.pages.length) {
        this.pageChanged.emit(page);
    }
  }
}
