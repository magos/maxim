import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable, of } from 'rxjs';

import { CharacterService } from '../../shared/character.service';
import { CharactersPage } from '../../shared/characters-page';
import { ListState } from '../../shared/list-state';
import { CharacterDelete } from '../../shared/character-delete';

@Component({
  selector: 'mx-characters-page',
  templateUrl: './characters-page.component.html',
  styleUrls: ['./characters-page.component.scss']
})
export class CharactersPageComponent implements OnInit {

  characters$: Observable<CharactersPage>;

  constructor(private characterService: CharacterService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.characters$ = of(routeData['characters']);
    });
  }

  fetchCharactersPage(pageNumber: number, searchTerm: string = ''): Observable<CharactersPage> {
    return this.characterService.getCharacters(pageNumber, searchTerm);
  }

  refreshData(listState: ListState) {
    this.characters$ = this.fetchCharactersPage(listState.currentPage, listState.searchTerm);
  }

  removeCharacter(deleteData: CharacterDelete): void {
    this.characterService.removeCharacter(deleteData.id)
      .subscribe(() => {
        this.refreshData(deleteData.listState);
      });
  }
}
