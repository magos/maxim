import { DebugElement } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CharactersPageComponent } from './characters-page.component';
import { TopbarMockComponent } from '../../../../testing/topbar-mock.component';
import { PaginationModule } from '../../../ui/pagination/pagination.module';
import { CharacterService } from '../../shared/character.service';
import { CharacterServiceStub } from 'src/testing/character.service.stub';
import { ActivatedRouteStub } from '../../../../testing/activated-route.stub';
import { CharactersPage } from '../../shared/characters-page';
import { ListViewMockComponent } from '../../../../testing/list-view-mock.component';
import { ListState } from '../../shared/list-state';
import { CharacterDelete } from '../../shared/character-delete';

describe('CharactersPageComponent', () => {
  const initialCharacters = [{
    id: 1,
    name: 'Luke Skywalker',
    species: 'Human',
    gender: 'male',
    homeworld: 'Tatooine'
  },
  {
    id: 2,
    name: 'C-3PO',
    species: 'Droid',
    gender: 'n/a',
    homeworld: 'Tatooine'
  },
  {
    id: 3,
    name: 'R2-D2',
    species: 'Droid',
    gender: 'n/a',
    homeworld: 'Naboo'
  }];
  const initialRouteData = { characters: initialCharacters };
  const activatedRoute: ActivatedRouteStub = new ActivatedRouteStub(initialRouteData);
  let component: CharactersPageComponent;
  let fixture: ComponentFixture<CharactersPageComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PaginationModule,
        RouterTestingModule
      ],
      declarations: [
        CharactersPageComponent,
        ListViewMockComponent,
        TopbarMockComponent
      ],
      providers: [
        { provide: CharacterService, useClass: CharacterServiceStub },
        { provide: ActivatedRoute, useValue: activatedRoute },
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersPageComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('fetchCharactersPage', () => {
    it('should fetch data from service with appropriate parameters', () => {
      const characterService = TestBed.get(CharacterService);
      spyOn(characterService, 'getCharacters');
      component.fetchCharactersPage(2, 'male');
      expect(characterService.getCharacters).toHaveBeenCalledTimes(1);
      expect(characterService.getCharacters).toHaveBeenCalledWith(2, 'male');
    });
  });

  describe('refreshData', () => {
    it('should refetch data with expected parameters ', () => {
      spyOn(component, 'fetchCharactersPage');
      const exampleListState: ListState = {
        currentPage: 1,
        pageSize: 10,
        searchTerm: ''
      };
      component.refreshData(exampleListState);
      expect(component.fetchCharactersPage).toHaveBeenCalledTimes(1);
      expect(component.fetchCharactersPage).toHaveBeenCalledWith(exampleListState.currentPage, exampleListState.searchTerm);
    });
  });
});
