import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Character } from '../../shared/character';

@Component({
  selector: 'mx-edit-character-page',
  templateUrl: './edit-character-page.component.html',
  styleUrls: ['./edit-character-page.component.scss']
})
export class EditCharacterPageComponent implements OnInit {

  character: Character;
  speciesDictionary: Array<string>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(routeData => {
      this.speciesDictionary = routeData['species'];
      this.character = routeData['character'] || {};
    });
  }
}
