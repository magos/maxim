import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { EditCharacterPageComponent } from './edit-character-page.component';
import { CharacterFormComponent } from '../../containers/character-form/character-form.component';
import { ActivatedRouteStub } from '../../../../testing/activated-route.stub';
import { CharacterService } from '../../shared/character.service';
import { CharacterServiceStub } from '../../../../testing/character.service.stub';

describe('EditCharacterPageComponent', () => {
  const initialRouteData = {
    species: [
      'Vulptereen',
      'Wookiee'
    ],
    character: {
      'id': 3,
      'name': 'R2-D2',
      'species': 'Droid',
      'gender': 'n/a',
      'homeworld': 'Naboo'
    }
  };
  const activatedRoute: ActivatedRouteStub = new ActivatedRouteStub(initialRouteData);
  let component: EditCharacterPageComponent;
  let fixture: ComponentFixture<EditCharacterPageComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [
        EditCharacterPageComponent,
        CharacterFormComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRoute },
        { provide: CharacterService, useClass: CharacterServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCharacterPageComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    activatedRoute.setParamMap({});
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
