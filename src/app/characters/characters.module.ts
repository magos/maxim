import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { EditCharacterPageComponent } from './pages/edit-character-page/edit-character-page.component';
import { CharacterService } from './shared/character.service';
import { PaginationModule } from '../ui/pagination/pagination.module';
import { CharacterListResolve } from './shared/character-list-resolve.service';
import { ListViewComponent } from './containers/list-view/list-view.component';
import { ListItemViewComponent } from './components/list-item-view/list-item-view.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { SearchModule } from '../ui/search/search.module';
import { SpeciesResolve } from './shared/species-resolve.service';
import { CharactersRoutingModule } from './characters-routing.module';
import { CharacterFormComponent } from './containers/character-form/character-form.component';
import { CharacterResolve } from './shared/character-resolve.service';

@NgModule({
  imports: [
    CommonModule,
    CharactersRoutingModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    PaginationModule,
    SearchModule
  ],
  declarations: [
    CharactersPageComponent,
    EditCharacterPageComponent,
    ListViewComponent,
    ListItemViewComponent,
    TopbarComponent,
    CharacterFormComponent
  ],
  providers: [
    CharacterService,
    CharacterListResolve,
    SpeciesResolve,
    CharacterResolve
  ]
})
export class CharactersModule { }
