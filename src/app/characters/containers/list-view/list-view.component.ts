import { Component, Input, Output, EventEmitter } from '@angular/core';

import { ListState } from '../../shared/list-state';
import { CharactersPage } from '../../shared/characters-page';
import { CharacterDelete } from '../../shared/character-delete';

@Component({
  selector: 'mx-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent {

  @Input() characters: CharactersPage;
  @Output() itemDeleted: EventEmitter<CharacterDelete>;
  @Output() stateChanged: EventEmitter<ListState>;
  listState: ListState;

  constructor() {
    this.listState = {
      currentPage: 1,
      pageSize: 10,
      searchTerm: ''
    };
    this.itemDeleted = new EventEmitter<CharacterDelete>();
    this.stateChanged = new EventEmitter<ListState>();
  }

  pageChanged(newPage: number) {
    this.listState.currentPage = newPage;
    this.stateChanged.emit(this.listState);
  }

  applyFilter(searchTerm: string) {
    this.listState.currentPage = 1;
    this.listState.searchTerm = searchTerm;
    this.stateChanged.emit(this.listState);
  }

  deleteItem(id: number) {
    this.itemDeleted.emit({
      id,
      listState: this.listState
    });
  }
}
