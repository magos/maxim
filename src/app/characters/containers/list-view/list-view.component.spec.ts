import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ListViewComponent } from './list-view.component';
import { TopbarMockComponent } from '../../../../testing/topbar-mock.component';
import { ListItemViewComponent } from '../../components/list-item-view/list-item-view.component';
import { PaginationModule } from '../../../ui/pagination/pagination.module';

describe('ListViewComponent', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        PaginationModule,
        RouterTestingModule
      ],
      declarations: [
        ListViewComponent,
        ListItemViewComponent,
        TopbarMockComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
