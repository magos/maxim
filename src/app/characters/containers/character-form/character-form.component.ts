import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Character } from '../../shared/character';
import { CharacterService } from '../../shared/character.service';

@Component({
  selector: 'mx-character-form',
  templateUrl: './character-form.component.html',
  styleUrls: ['./character-form.component.scss']
})
export class CharacterFormComponent implements OnInit {

  @Input() character: Character;
  @Input() speciesDictionary: Array<string>;
  @ViewChild('formElement') formElement: ElementRef;
  characterForm: FormGroup;
  loading: boolean;
  submittedInvalid: boolean;

  get name(): AbstractControl {
    return this.characterForm.get('name');
  }

  get species(): AbstractControl {
    return this.characterForm.get('species');
  }

  constructor(private fb: FormBuilder,
    private router: Router,
    private characterService: CharacterService) {
      this.submittedInvalid = false;
    }

  ngOnInit() {
    this.characterForm = this.initializeForm(this.character);
  }

  initializeForm(character: Character): FormGroup {
    return this.fb.group({
      name: [character.name || '', Validators.required],
      species: [character.species || '', Validators.required],
      gender: [character.gender || '', Validators.required],
      homeworld: [character.homeworld || '']
    });
  }

  submit(characterForm: FormGroup) {
    if (characterForm.valid) {
      this.loading = true;
      this.upsertCharacter(characterForm.value);
    } else {
      this.focusFirstInvalid();
      this.submittedInvalid = true;
    }
  }

  upsertCharacter(character: Character): void {
    character.id = this.character.id;
    this.characterService.upsertCharacter(character).subscribe(() => {
      this.loading = false;
      this.router.navigate(['/']);
    }, () => {
      this.loading = false;
    });
  }

  focusFirstInvalid(): void {
    const invalidControls = this.formElement.nativeElement.querySelectorAll('.ng-invalid');
    (<HTMLInputElement>invalidControls[0]).focus();
  }
}
