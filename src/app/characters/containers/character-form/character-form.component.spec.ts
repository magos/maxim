import { DebugElement } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';

import { CharacterFormComponent } from './character-form.component';
import { RouterStub } from '../../../../testing/router.stub';
import { CharacterService } from '../../shared/character.service';
import { CharacterServiceStub } from 'src/testing/character.service.stub';
import { Character } from '../../shared/character';
import { getFakeCharacterForm } from '../../../../testing/utils';

describe('CharacterFormComponent', () => {
  let component: CharacterFormComponent;
  let fixture: ComponentFixture<CharacterFormComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [ CharacterFormComponent ],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: CharacterService, useClass: CharacterServiceStub }
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterFormComponent);
    component = fixture.componentInstance;
    component.speciesDictionary = [
      'Ewok',
      'Geonosian',
      'Gungan',
      'Human',
      'Hutt',
      'Iktotchi',
      'Kaleesh',
      'Kaminoan'
    ];
    de = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize the character form', () => {
    spyOn(component, 'initializeForm').and.returnValue(getFakeCharacterForm());
    fixture.detectChanges();
    expect(component.initializeForm).toHaveBeenCalledTimes(1);
  });

  it('should initialize name field as required', () => {
    const characterForm = component.initializeForm({} as Character);
    expect(characterForm.controls['name'].errors).toEqual({ required: true });
  });

  it('should initialize species field as required', () => {
    const characterForm = component.initializeForm({} as Character);
    expect(characterForm.controls['species'].errors).toEqual({ required: true });
  });

  it('should initialize gender field as required', () => {
    const characterForm = component.initializeForm({} as Character);
    expect(characterForm.controls['gender'].errors).toEqual({ required: true });
  });

  it('should initialize homeworld field as optional', () => {
    const characterForm = component.initializeForm({} as Character);
    expect(characterForm.controls['homeworld'].errors).toBeNull();
  });

  describe('with existing character', () => {
    beforeEach(() => {
      component.character = {
        id: 14,
        name: 'Qui-Gon Jinn',
        species: 'Human',
        gender: 'male',
        homeworld: 'Earth'
      };
      fixture.detectChanges();
    });

    it('should initialize the form with provided data', () => {
      expect(component.characterForm.value).toEqual({
        name: 'Qui-Gon Jinn',
        species: 'Human',
        gender: 'male',
        homeworld: 'Earth'
      });
    });

    describe('submit', () => {
      it('should submit form when all required fields has been filled in', () => {
        spyOn(component, 'upsertCharacter');
        component.characterForm.patchValue({ name: 'Qui-Gon Jinn 2' });
        component.submit(component.characterForm);
        expect(component.upsertCharacter).toHaveBeenCalledWith({
          name: 'Qui-Gon Jinn 2',
          species: 'Human',
          gender: 'male',
          homeworld: 'Earth'
        });
      });

      it('should not submit form with empty name field', () => {
        spyOn(component, 'upsertCharacter');
        component.characterForm.patchValue({ name: '' });
        fixture.detectChanges();
        component.submit(component.characterForm);
        expect(component.upsertCharacter).not.toHaveBeenCalled();
        expect(component.submittedInvalid).toBeTruthy();
      });

      it('should not submit form with empty species field', () => {
        spyOn(component, 'upsertCharacter');
        component.characterForm.patchValue({ species: '' });
        fixture.detectChanges();
        component.submit(component.characterForm);
        expect(component.upsertCharacter).not.toHaveBeenCalled();
        expect(component.submittedInvalid).toBeTruthy();
      });

      it('should not submit form with empty gender field', () => {
        spyOn(component, 'upsertCharacter');
        component.characterForm.patchValue({ gender: '' });
        fixture.detectChanges();
        component.submit(component.characterForm);
        expect(component.upsertCharacter).not.toHaveBeenCalled();
        expect(component.submittedInvalid).toBeTruthy();
      });

      it('should submit form despite of empty homeworld field', () => {
        spyOn(component, 'upsertCharacter');
        component.characterForm.patchValue({ homeworld: '' });
        fixture.detectChanges();
        component.submit(component.characterForm);
        expect(component.upsertCharacter).toHaveBeenCalledTimes(1);
        expect(component.submittedInvalid).toBeFalsy();
      });
    });

    describe('upsertCharacter', () => {
      it('should call character service', () => {
        const characterService = TestBed.get(CharacterService);
        spyOn(characterService, 'upsertCharacter').and.returnValue(of());
        component.upsertCharacter(component.character);
        expect(characterService.upsertCharacter).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('with empty character', () => {
    beforeEach(() => {
      component.character = {} as Character;
      fixture.detectChanges();
    });

    it('should initialize the form with empty values', () => {
      expect(component.characterForm.value).toEqual({
        name: '',
        species: '',
        gender: '',
        homeworld: ''
      });
    });

    describe('submit', () => {
      it('should focus first invalid field', () => {
        spyOn(component, 'focusFirstInvalid');
        component.submit(component.characterForm);
        expect(component.focusFirstInvalid).toHaveBeenCalledTimes(1);
      });
    });
  });
});
