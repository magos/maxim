export interface ListState {
  currentPage: number;
  pageSize: number;
  searchTerm: string;
}
