import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Character } from './character';
import { environment } from '../../../environments/environment';
import { CharactersPage } from './characters-page';

@Injectable()
export class CharacterService {

  constructor(private http: HttpClient) {}

  getCharacters(page: number = 1, searchTerm: string = ''): Observable<CharactersPage> {
    return this.http.get<Array<Character>>(`${environment.baseApiUrl}/characters`,
      { params: { '_page': page.toString(), 'q': searchTerm }, observe: 'response' })
      .pipe(
        map((resp: HttpResponse<Array<Character>>) => ({
          data: resp.body,
          total: +resp.headers.get('X-Total-Count')
        })),
        catchError(this.formatErrors));
  }

  getCharacterById(id: number): Observable<Character> {
    return this.http.get<Character>(`${environment.baseApiUrl}/characters/${id}`)
      .pipe(catchError(this.formatErrors));
  }

  upsertCharacter(character: Character): Observable<Character> {
    return character.id ? this.updateCharacter(character) : this.insertCharacter(character);
  }

  insertCharacter(character: Character): Observable<Character> {
    return this.http.post<Character>(`${environment.baseApiUrl}/characters`, character)
      .pipe(catchError(this.formatErrors));
  }

  updateCharacter(character: Character): Observable<Character> {
    return this.http.put<Character>(`${environment.baseApiUrl}/characters/${character.id}`, character)
      .pipe(catchError(this.formatErrors));
  }

  removeCharacter(id: number): Observable<Character> {
    return this.http.delete<Character>(`${environment.baseApiUrl}/characters/${id}`)
      .pipe(catchError(this.formatErrors));
  }

  getSpecies(): Observable<Array<string>> {
    return this.http.get<Array<string>>(`${environment.baseApiUrl}/species`)
      .pipe(catchError(this.formatErrors));
  }

  private formatErrors(error: any) {
    return throwError(error.error);
  }
}
