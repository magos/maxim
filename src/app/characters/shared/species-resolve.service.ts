import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';

import { CharacterService } from './character.service';

@Injectable()
export class SpeciesResolve implements Resolve<Array<string>> {

  constructor(private characterService: CharacterService) { }

  resolve(): Observable<Array<string>> {
    return this.characterService.getSpecies();
  }
}
