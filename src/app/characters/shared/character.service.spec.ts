import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CharacterService } from './character.service';
import { CharactersPage } from './characters-page';
import { environment } from '../../../environments/environment';
import { Character } from './character';

describe('CharacterService', () => {
  let service: CharacterService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [CharacterService]
    });
    service = TestBed.get(CharacterService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getCharacters should GET expected list of characters', () => {
    const expectedCharacters: Array<Character> = [{
        id: 18,
        name: 'Kylo Ren',
        species: 'Human',
        gender: 'male',
        homeworld: ''
      }, {
        id: 17,
        name: 'Poe Dameron',
        species: 'Human',
        gender: 'male',
        homeworld: ''
      }
    ];

    service.getCharacters().subscribe((charactersPage: CharactersPage) => {
      expect(charactersPage.data.length).toBe(2);
      expect(charactersPage.total).toBe(2);
      expect(charactersPage.data).toEqual(expectedCharacters);
    });

    const req = httpMock.expectOne(`${environment.baseApiUrl}/characters?_page=1&q=`);
    expect(req.request.method).toBe('GET');
    req.flush(expectedCharacters, { headers: {'X-Total-Count': '2' } });
  });

  it('getCharacterById should GET expected character', () => {
    const expectedCharacter: Character = {
      id: 17,
      name: 'Poe Dameron',
      species: 'Human',
      gender: 'male',
      homeworld: ''
    };

    service.getCharacterById(17).subscribe((character: Character) => {
      expect(character).toEqual(expectedCharacter);
    });

    const req = httpMock.expectOne(`${environment.baseApiUrl}/characters/17`);
    expect(req.request.method).toBe('GET');
    req.flush(expectedCharacter);
  });

  it('insertCharacter should POST expected character', () => {
    const newCharacter: Character = {
      name: 'Poe Dameron',
      species: 'Human',
      gender: 'male',
      homeworld: ''
    } as Character;

    service.insertCharacter(newCharacter).subscribe((addedCharacter: Character) => {
      expect(addedCharacter).toEqual(newCharacter);
    });

    const req = httpMock.expectOne(`${environment.baseApiUrl}/characters`);
    expect(req.request.method).toBe('POST');
    req.flush(newCharacter);
  });

  it('updateCharacter should PUT specified character', () => {
    const characterToUpdate: Character = {
      id: 17,
      name: 'Poe Dameron 2',
      species: 'Human',
      gender: 'female',
      homeworld: ''
    };

    service.updateCharacter(characterToUpdate).subscribe((updatedCharacter: Character) => {
      expect(updatedCharacter).toEqual(characterToUpdate);
    });

    const req = httpMock.expectOne(`${environment.baseApiUrl}/characters/17`);
    expect(req.request.method).toBe('PUT');
    req.flush(characterToUpdate);
  });

  it('removeCharacter should DELETE specified character', () => {
    const characterToRemove: Character = {
      id: 17,
      name: 'Poe Dameron 2',
      species: 'Human',
      gender: 'female',
      homeworld: ''
    };
    service.removeCharacter(characterToRemove.id).subscribe((deletedCharacter: Character) => {
      expect(deletedCharacter).toEqual(characterToRemove);
    });

    const req = httpMock.expectOne(`${environment.baseApiUrl}/characters/17`);
    expect(req.request.method).toBe('DELETE');
    req.flush(characterToRemove);
  });

  it('getSpecies should GET expected list of species', () => {
    const expectedSpecies: Array<string> = [
      'Aleena',
      'Besalisk',
      'Droid'
    ];

    service.getSpecies().subscribe((species: Array<string>) => {
      expect(species.length).toBe(3);
      expect(species).toEqual(expectedSpecies);
    });

    const req = httpMock.expectOne(`${environment.baseApiUrl}/species`);
    expect(req.request.method).toBe('GET');
    req.flush(expectedSpecies);
  });

  describe('upsertCharacter', () => {
    beforeEach(() => {
      spyOn(service, 'updateCharacter');
      spyOn(service, 'insertCharacter');
    });

    it('should update character when called with character with specified identifier', () => {
      const existingCharacter: Character = {
        id: 17,
        name: 'Poe Dameron',
        species: 'Human',
        gender: 'male',
        homeworld: ''
      };
      service.upsertCharacter(existingCharacter);
      expect(service.updateCharacter).toHaveBeenCalledWith(existingCharacter);
    });

    it('should insert character when called with character without identifier defined', () => {
      const newCharacter: Character = {
        name: 'Poe Dameron',
        species: 'Human',
        gender: 'male',
        homeworld: ''
      } as Character;
      service.upsertCharacter(newCharacter);
      expect(service.insertCharacter).toHaveBeenCalledWith(newCharacter);
    });
  });
});
