import { TestBed } from '@angular/core/testing';

import { SpeciesResolve } from './species-resolve.service';
import { CharacterService } from './character.service';
import { CharacterServiceStub } from '../../../testing/character.service.stub';

describe('SpeciesResolve', () => {
  let resolver: SpeciesResolve;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SpeciesResolve,
        { provide: CharacterService, useClass: CharacterServiceStub }
      ]
    });
    resolver = TestBed.get(SpeciesResolve);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should get species while resolving', () => {
    const characterService = TestBed.get(CharacterService);
    spyOn(characterService, 'getSpecies');
    resolver.resolve();
    expect(characterService.getSpecies).toHaveBeenCalledWith();
  });
});
