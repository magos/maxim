import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

import { Character } from './character';
import { CharacterService } from './character.service';

@Injectable()
export class CharacterResolve implements Resolve<Character> {

  constructor(private characterService: CharacterService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Character> {
    return this.characterService.getCharacterById(+route.params['id']);
  }
}
