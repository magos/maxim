import { TestBed } from '@angular/core/testing';

import { CharacterListResolve } from './character-list-resolve.service';
import { CharacterService } from './character.service';
import { CharacterServiceStub } from '../../../testing/character.service.stub';

describe('CharacterListResolve', () => {
  let resolver: CharacterListResolve;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CharacterListResolve,
        { provide: CharacterService, useClass: CharacterServiceStub }
      ]
    });
    resolver = TestBed.get(CharacterListResolve);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should get characters while resolving', () => {
    const characterService = TestBed.get(CharacterService);
    spyOn(characterService, 'getCharacters');
    resolver.resolve();
    expect(characterService.getCharacters).toHaveBeenCalledWith();
  });
});
