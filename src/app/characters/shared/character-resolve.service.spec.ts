import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot } from '@angular/router';

import { CharacterResolve } from './character-resolve.service';
import { CharacterService } from './character.service';
import { CharacterServiceStub } from '../../../testing/character.service.stub';

describe('CharacterResolve', () => {
  let resolver: CharacterResolve;
  let route: ActivatedRouteSnapshot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CharacterResolve,
        { provide: CharacterService, useClass: CharacterServiceStub }
      ]
    });
    resolver = TestBed.get(CharacterResolve);
    route = new ActivatedRouteSnapshot();
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('should get character by identifier while resolving', () => {
    const characterService = TestBed.get(CharacterService);
    route.params = { id: '9' };
    spyOn(characterService, 'getCharacterById');
    resolver.resolve(route);
    expect(characterService.getCharacterById).toHaveBeenCalledWith(9);
  });
});
