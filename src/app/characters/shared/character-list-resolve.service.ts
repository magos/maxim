import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';

import { CharactersPage } from './characters-page';
import { CharacterService } from './character.service';

@Injectable()
export class CharacterListResolve implements Resolve<CharactersPage> {

  constructor(private characterService: CharacterService) { }

  resolve(): Observable<CharactersPage> {
    return this.characterService.getCharacters();
  }
}
