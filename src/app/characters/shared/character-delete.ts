import { ListState } from './list-state';

export interface CharacterDelete {
  id: number;
  listState: ListState;
}
