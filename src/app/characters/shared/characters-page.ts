import { Character } from './character';

export interface CharactersPage {
  data: Array<Character>;
  total: number;
}
