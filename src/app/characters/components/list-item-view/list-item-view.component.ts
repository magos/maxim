import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Character } from '../../shared/character';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tr[mx-list-item-view]',
  templateUrl: './list-item-view.component.html',
  styleUrls: ['./list-item-view.component.scss']
})
export class ListItemViewComponent {
  @Input() character: Character;
  @Output() remove: EventEmitter<number>;

  constructor() {
    this.remove = new EventEmitter<number>();
  }
}
