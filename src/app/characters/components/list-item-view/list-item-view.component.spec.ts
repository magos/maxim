import { DebugElement } from '@angular/core';
import { Location } from '@angular/common';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ListItemViewComponent } from './list-item-view.component';
import { DummyComponent } from '../../../../testing/dummy.component';

describe('ListItemViewComponent', () => {
  let component: ListItemViewComponent;
  let fixture: ComponentFixture<ListItemViewComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([
          { path: 'edit/:id', component: DummyComponent }
        ])],
      declarations: [ ListItemViewComponent, DummyComponent ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemViewComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    component.character = {
      id: 18,
      name: 'Darth Vader',
      species: 'Human',
      gender: 'male',
      homeworld: 'Tatooine'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render character id', () => {
    const idCell: DebugElement = de.query(By.css('th'));
    expect(idCell.nativeElement.textContent).toBe('18');
  });

  it('should render character name', () => {
    const tableCells: DebugElement[] = de.queryAll(By.css('td'));
    expect(tableCells[0].nativeElement.textContent).toBe('Darth Vader');
  });

  it('should render character species', () => {
    const tableCells: DebugElement[] = de.queryAll(By.css('td'));
    expect(tableCells[1].nativeElement.textContent).toBe('Human');
  });

  it('should render character gender', () => {
    const tableCells: DebugElement[] = de.queryAll(By.css('td'));
    expect(tableCells[2].nativeElement.textContent).toBe('male');
  });

  it('should render character homeworld', () => {
    const tableCells: DebugElement[] = de.queryAll(By.css('td'));
    expect(tableCells[3].nativeElement.textContent).toBe('Tatooine');
  });

  it('should properly compose edit link', () => {
    const anchorReference = de.query(By.css('#mx-edit-anchor')).nativeElement.getAttribute('href');
    expect(anchorReference).toBe('/edit/18');
  });

  it('should navigate to appropriate location after anchor click', fakeAsync(() => {
    const location = TestBed.get(Location);
    de.query(By.css('#mx-edit-anchor')).nativeElement.click();
    tick();
    expect(location.path()).toEqual('/edit/18');
  }));

  it('should emit remove event with proper identifier after remove button click', () => {
    component.remove.subscribe((id: number) => {
      expect(id).toEqual(18);
    });
    de.query(By.css('#mx-remove-button')).nativeElement.click();
  });
});
