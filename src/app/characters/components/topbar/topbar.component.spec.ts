import { DebugElement } from '@angular/core';
import { Location } from '@angular/common';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TopbarComponent } from './topbar.component';
import { SearchModule } from '../../../ui/search/search.module';
import { DummyComponent } from '../../../../testing/dummy.component';

describe('TopbarComponent', () => {
  let component: TopbarComponent;
  let fixture: ComponentFixture<TopbarComponent>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SearchModule,
        RouterTestingModule.withRoutes([
        { path: 'add', component: DummyComponent }
      ])],
      declarations: [ TopbarComponent, DummyComponent ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render search component', () => {
    const searchComponent = de.query(By.css('mx-search'));
    expect(searchComponent).toBeTruthy();
  });

  it('should navigate to appropriate location after \'Add New\' anchor click', fakeAsync(() => {
    const location = TestBed.get(Location);
    de.query(By.css('#mx-add-anchor')).nativeElement.click();
    tick();
    expect(location.path()).toEqual('/add');
  }));
});
