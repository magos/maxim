import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mx-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent {

  @Output() applyFilter: EventEmitter<string>;

  constructor() {
    this.applyFilter = new EventEmitter<string>();
  }
}
