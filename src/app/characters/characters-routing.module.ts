import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { EditCharacterPageComponent } from './pages/edit-character-page/edit-character-page.component';
import { CharacterListResolve } from './shared/character-list-resolve.service';
import { SpeciesResolve } from './shared/species-resolve.service';
import { CharacterResolve } from './shared/character-resolve.service';

const routes: Routes = [
  {
    path: '',
    component: CharactersPageComponent,
    resolve: { characters: CharacterListResolve }
  },
  {
    path: 'add',
    component: EditCharacterPageComponent,
    resolve: { species: SpeciesResolve }
  },
  {
    path: 'edit/:id',
    component: EditCharacterPageComponent,
    resolve: { character: CharacterResolve, species: SpeciesResolve } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CharactersRoutingModule { }
