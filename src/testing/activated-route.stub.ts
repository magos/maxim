import { Type } from '@angular/core';
import { convertToParamMap, ParamMap, Params, ActivatedRoute,
  ActivatedRouteSnapshot, UrlSegment, Data, Route } from '@angular/router';

import { ReplaySubject, Observable, of } from 'rxjs';

export class ActivatedRouteStub {
  private subject = new ReplaySubject<ParamMap>();

  readonly paramMap = this.subject.asObservable();
  snapshot: ActivatedRouteSnapshot;
  url: Observable<UrlSegment[]>;
  params: Observable<Params>;
  queryParams: Observable<Params>;
  queryParamMap: Observable<ParamMap>;
  fragment: Observable<string>;
  data: Observable<Data>;
  outlet: string;
  component: Type<any>|string;
  routeConfig: Route;
  root: ActivatedRoute;
  parent: ActivatedRoute;
  firstChild: ActivatedRoute;
  children: ActivatedRoute[];
  pathFromRoot: ActivatedRoute[];
  toString(): string {
      return '';
  }

  constructor(initialData?: any) {
    this.data = of(initialData);
  }

  setParamMap(params?: Params) {
    this.subject.next(convertToParamMap(params));
  }
}
