import { Component, Input, EventEmitter, Output } from '@angular/core';

import { CharactersPage } from '../app/characters/shared/characters-page';
import { CharacterDelete } from '../app/characters/shared/character-delete';
import { ListState } from '../app/characters/shared/list-state';

@Component({
    selector: 'mx-list-view',
    template: ''
})
export class ListViewMockComponent {
  @Input() characters: CharactersPage;
  @Output() itemDeleted: EventEmitter<CharacterDelete>;
  @Output() stateChanged: EventEmitter<ListState>;

  constructor() {
    this.itemDeleted = new EventEmitter<CharacterDelete>();
    this.stateChanged = new EventEmitter<ListState>();
  }
}
