import { FormGroup, FormControl } from '@angular/forms';

export function getFakeCharacterForm(): FormGroup {
  return new FormGroup({
    name: new FormControl(),
    species: new FormControl(),
    gender: new FormControl(),
    homeworld: new FormControl()
  });
}
