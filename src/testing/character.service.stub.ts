import { of, Observable } from 'rxjs';

import { CharactersPage } from 'src/app/characters/shared/characters-page';
import { Character } from 'src/app/characters/shared/character';

export class CharacterServiceStub {
  getCharacters(page: number = 1, searchTerm: string = ''): Observable<CharactersPage> {
    return of({
      data: [
        {
          'id': 6,
          'name': 'Obi-Wan Kenobi',
          'species': 'Human',
          'gender': 'male',
          'homeworld': 'Stewjon'
        },
        {
          'id': 7,
          'name': 'Anakin Skywalker',
          'species': 'Human',
          'gender': 'male',
          'homeworld': 'Tatooine'
        },
        {
          'id': 8,
          'name': 'Chewbacca',
          'species': 'Wookiee',
          'gender': 'male',
          'homeworld': 'Kashyyyk'
        },
        {
          'id': 9,
          'name': 'Han Solo',
          'species': 'Human',
          'gender': 'male',
          'homeworld': 'Corellia'
        }
      ],
      total: 4
    });
  }

  getCharacterById(id: number): Observable<Character> {
    return of({
      'id': 9,
      'name': 'Han Solo',
      'species': 'Human',
      'gender': 'male',
      'homeworld': 'Corellia'
    });
  }

  getSpecies(): Observable<Array<string>> {
    return of([
      'Mon Calamari',
      'Muun',
      'Nautolan',
      'Neimodian',
      'Quermian',
      'Rodian',
    ]);
  }

  removeCharacter(id: number): Observable<Character> {
    return of({
      'id': 8,
      'name': 'Han Solo',
      'species': 'Human',
      'gender': 'male',
      'homeworld': 'Corellia'
    });
  }

  upsertCharacter(character: Character): Observable<Character> {
    return of({
      'id': 8,
      'name': 'Han Solo',
      'species': 'Human',
      'gender': 'male',
      'homeworld': 'Corellia'
    });
  }
}
