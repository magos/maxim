# Maxim - Angular demo application
author: Marcin Gościniak

The app is built using [angular-cli](https://github.com/angular/angular-cli). See its documentation for more details.

## General idea

Regarding copyrights, it's tricky to present the applications I worked on the most of the time. That's why I prepared mini
web app - Maxim, which is designed to show the way I used to code. Don't bother with the feature - which is super
minimalistic. The point is to show:
* how I organize the code
* how I separate concernes
* how I test code
* how I use Angular features to make things as easy as possible
* how I care of readability and testability (naming, clean code, linting, conventions)

## Launching

Scripts:
```bash
npm start # run the app using webpack-dev-server
npm run api # start the json-server
npm run lint # lint the code using es-lint
npm test # run the unit tests
npm run e2e # run the e2e tests
```

## Requirements
UI is connected to the API served by json-server on [http://localhost:3000]. Start API by command `npm run api`.
The home view displays a list of characters.

### Pagination
User is able to change a page of partly visible results to see another part.
* Pagination is arrange server-side
* Results display in pages of ten items
* Click on page button changes current page to the selected one
* Next to both edges of the pagination bar - there are previous and next buttons. They change current page by one (in relevant direction). 
* Next button is disabled if currently selected page is the last one
* Previous button is disabled if currently selected page is the first one

### Searching
User is able to filter the list of characters using search bar above.
* Filtering uses full-text search (which means searching thoughout all columns)
* In case of no matching results - appropriate message is displayed
* Search request to API are debounced by 200ms and distinct until search term changes.
* When the search bar is empty - regular list of items is shown

### Adding character
User is able to add new character to the list
* Separate route for adding character is available
* User is getting there when click on Add new button
* Form consists of:
  * Name input (required field)
  * Species select (required field) (options for select comes from separate API endpoint: [/species](http://localhost:3000/species)
  * Gender - radio input group (required field) (Options: male, female, n/a)
  * Homeworld input (optional field)
* Required fields are marked by blue asterisks
* Appropriate validation message is displayed if value of the field is invalid and already touched by user
* Validation feedback (input styling and message) for all fields is displayed when user tries to send invalid form
* If a user tries to submit the form but the form is invalid, the top-most invalid form field gets focused.
* Submit button is disabled only when the request creating a new item is in progress.
* Submit a valid form causes POST request
* Successful request results in automatic navigation to the character's list

### Editing character
User is able to edit character available on the list
* Separate route for adding character is available
* Editing uses the same component, but passed character object contains identifier
* User is getting there when click on Edit button on ther list next to the character
* Similar validation rules as in case od adding apply to editing mode
* Submit a valid form causes PUT request
* Successful request results in automatic navigation to the character's list

### Deleting character
* Delete feature is available from the level of list view (Delete button)
* Click on the button causes DELETE request
* The character' list refreshes automatically on deletion success
